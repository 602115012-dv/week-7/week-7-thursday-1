import React from 'react';
import MainRouting from './component/routing/MainRouting';
import './App.css';

function App() {
  return (
    <div>
      <MainRouting />
    </div>
  );
}

export default App;
