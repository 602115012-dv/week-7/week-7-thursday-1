import React, { useState, useEffect } from 'react';
import { Row, Col, Typography, List } from 'antd';

const UserAlbumList = (props) => {
  const [UserAlbums, setUserAlbums] = useState([]);

  useEffect(() => {
    fetchUserAlbums();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const fetchUserAlbums = () => {
    (async () => {
      try {
        let response = await fetch(`https://jsonplaceholder.typicode.com/albums?userId=${props.match.params.userId}`);
        let data = await response.json();
        console.log(data);

        setUserAlbums(data);
      } catch(err) {
        console.log(err);
      }
    })();
  }

  return (
    <div>
      <Row type="flex" justify="center">
        <Col span={16} className="content padding-20">
          <Typography.Title className="text-align-center">
            Albums
          </Typography.Title>
          <List
            bordered
            dataSource={UserAlbums}
            renderItem={item => 
            <List.Item>
              <a href={`/users/${item.userId}/albums/${item.id}`}>
                {item.title}
              </a>
            </List.Item>}
          />
        </Col>
      </Row>
    </div>
  );
}

export default UserAlbumList;