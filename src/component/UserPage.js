import React, { useState, useEffect } from 'react';
import { Row, Col, Typography, Table, Input, Divider } from 'antd';

const { Column } = Table;

const UserPage = () => {
  const [users, setUsers] = useState([]);
  const [filters, setFilters] = useState([
    {
      "name": "userName",
      "value": "",
      "function": (user) => {
        let value = filters[getFilterIndex("userName")].value.toUpperCase();
        return user.name.toUpperCase().includes(value);
      }
    }
  ]);
  
  useEffect(() => {
    fetchAllUsers();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const fetchAllUsers = () => {
    (async () => {
      try {
        let response = await fetch('https://jsonplaceholder.typicode.com/users');
        let data = await response.json();
        setUsers(data);
      } catch(err) {
        console.log(err);
      }
    })();
  }

  const filter = (data) => {
    let filteredData = data;

    filters.forEach(filter => {
      filteredData = filteredData.filter(filter.function);
    });

    return filteredData;
  }

  const setFilterValue = (key, value) => {
    let tmpFilters = [...filters];
    tmpFilters[getFilterIndex(key)].value = value;
    setFilters(tmpFilters);
  }

  const getFilterIndex = (name) => {
    return filters.findIndex(element => {
      return element.name === name;
    })
  }

  return (
    <div>
      <Row type="flex" justify="center">
        <Col span={16} className="content padding-20">
          <Typography.Title className="text-align-center">Users</Typography.Title>
          <Input placeholder="Filter name" onKeyUp={(event) => 
            setFilterValue("userName", event.target.value)} 
          />
          <Table 
            dataSource={filter(users)} 
            rowKey="id" 
            loading={users.length === 0}
          >
            <Column title="Id" dataIndex="id" />
            <Column title="Name" dataIndex="name" />
            <Column title="Email" dataIndex="email" />
            <Column title="Action" render={
              (data) => {
                return (
                  <span>
                    <a href={`/users/${data.id}/todo`}>See todo list</a>
                    <Divider type="vertical" />
                    <a href={`/users/${data.id}/albums`}>See albums</a>
                  </span>
                );
              }
            }/>
          </Table>
        </Col>
      </Row>
    </div>
  );
}

export default UserPage;