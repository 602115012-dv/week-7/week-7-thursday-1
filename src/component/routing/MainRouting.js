import React from 'react';
import { BrowserRouter, Route, Redirect } from 'react-router-dom';
import UserPage from '../UserPage';
import UserTaskList from '../UserTodoList';
import NavBar from '../NavBar';
import UserAlbumList from '../UserAlbumList';
import UserAlbum from '../UserAlbum';

const MainRouting = () => {
  return (
    <BrowserRouter>
      <Route exact path="/" render={() => (
        <Redirect to="/users"/>
      )}/>
      <Route path="/" component={NavBar} />
      <Route exact={true} path="/users" component={UserPage} />
      <Route path="/users/:userId/todo" component={UserTaskList} />
      <Route exact={true} path="/users/:userId/albums" component={UserAlbumList} />
      <Route path="/users/:userId/albums/:albumId" component={UserAlbum} />
    </BrowserRouter>
  );
}

export default MainRouting;