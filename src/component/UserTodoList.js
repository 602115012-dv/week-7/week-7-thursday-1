import React, { useState, useEffect } from 'react';
import { Row, Col, Typography, Table, Tag, Button, Select } from 'antd';
import TaskStatus from './entity/TaskStatus';

const { Column } = Table;
const { Option } = Select;

const UserTaskList = (props) => {
  const [taskList, setTaskList] = useState([]);
  const [filters, setFilters] = useState([
    {
      "name": "taskStatus",
      "value": "All",
      "function": (task) => {
        let value = filters[getFilterIndex("taskStatus")].value;
        if (value === "All") {
          return true;
        } else {
          let status = value === TaskStatus.DONE ? true : false;
          return task["completed"] === status;
        }
      }
    }
  ]);

  useEffect(() => {
    fetchUserTasks();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const fetchUserTasks = () => {
    (async () => {
      try {
        let response = await fetch(`https://jsonplaceholder.typicode.com/todos?userId=${props.match.params.userId}`);
        let data = await response.json();
        setTaskList(data);
      } catch(err) {
        console.log(err);
      }
    })();
  }

  const renderTitle = (data) => {
    return (
      <Typography.Text delete={data.completed}>{data.title}</Typography.Text>
    );
  }

  const renderStatus = (data) => {
    return (
      <Tag 
      color={data.completed ? "green" : "blue"}>
        {data.completed ? "Done" : "Doing"}
      </Tag>
    );
  }

  const updateTask = (task, taskStatus) => {
    let tmpTaskList = [...taskList];
    let taskIndex = tmpTaskList.findIndex((item) => {
      return item.id === task.id;
    });

    switch (taskStatus) {
      case TaskStatus.DONE:
        tmpTaskList[taskIndex].completed = true;
        break;
      case TaskStatus.DOING:
        tmpTaskList[taskIndex].completed = false;
        break;
      case TaskStatus.REMOVE:
        tmpTaskList.splice(taskIndex, 1);
        break;
      default:
        break;
    }

    setTaskList(tmpTaskList);
  }

  const renderButtons = (data) => {
    if (data.completed) {
      return (
        <span>
          <Button type="primary" onClick={() => updateTask(data, TaskStatus.DOING)}>Reopen</Button>
          <Button type="danger" onClick={() => updateTask(data, TaskStatus.REMOVE)}>Remove</Button>
        </span>
      );
    }
    return (
      <Button type="primary" onClick={() => updateTask(data, TaskStatus.DONE)}>Done</Button>
    );
  }

  const filter = (data) => {
    let filteredData = data;

    filters.forEach(filter => {
      filteredData = filteredData.filter(filter.function);
    });

    return filteredData;
  }

  const setFilterValue = (key, value) => {
    let tmpFilters = [...filters];
    tmpFilters[getFilterIndex(key)].value = value;
    setFilters(tmpFilters);
  }

  const getFilterIndex = (name) => {
    return filters.findIndex(element => {
      return element.name === name;
    })
  }

  return (
    <div>
      <Row type="flex" justify="center">
        <Col span={16} className="content padding-20">
          <Typography.Title className="text-align-center">
            Task list
          </Typography.Title>
          <Select defaultValue="All" style={{ width: 120 }} onChange={(value) => {
              setFilterValue("taskStatus", value);
            }}>
            <Option value="All">All</Option>
            <Option value={TaskStatus.DOING}>Doing</Option>
            <Option value={TaskStatus.DONE}>Done</Option>
          </Select>
          <Table 
            dataSource={filter(taskList)} 
            rowKey="id"
            loading={taskList.length === 0}
          >
            <Column title="Task id" dataIndex="id" />
            <Column title="Title" render={data => renderTitle(data)} />
            <Column title="Status" render={data => renderStatus(data)} />
            <Column title="Action" render={data => renderButtons(data)} />
          </Table>
        </Col>
      </Row>
    </div>
  );
}

export default UserTaskList;